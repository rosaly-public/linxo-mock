# Disabled because pylint does not like upper case do_GET
# but we have to do so, otherwise it's not mapped to HTTP
# verb GET etc.
#
# pylint: disable=invalid-name
from typing import Union
from typing import Optional
from typing import List
from typing import Dict

import base64
import json as json_module
from urllib.parse import parse_qsl
import os
import uuid
from pathlib import Path
from string import Template
import time
from decimal import Decimal

from http.server import BaseHTTPRequestHandler

CLIENT_ID = os.environ["MOCK_LINXO_CLIENT_ID"]
CLIENT_SECRET = os.environ["MOCK_LINXO_CLIENT_SECRET"]

DATA_STORE: Dict = {}
USERS_STORE: Dict = {}
ACCESS_TOKENS: Dict = {}

class MockHandler(BaseHTTPRequestHandler):
    """ Handle all requests as if it was a Linxo instance
    """


    def do_GET(self):
        """Answer to GET methods
        """
        if self.path.startswith('/v2.1/providers'):
            # dump of the real API made the 2023-06-07
            with Path(__file__).with_name('providers.json').open('r') as f:
                self._send_raw_json_response(f.read().encode('utf-8'))
            return

        # This URL is loaded directly by the browser and will display
        # the linxo Iframe
        if self.path.startswith('/v2.1/widget/add_connection'):
            # we transform the query part of the url into a python dict
            query_string = self.path.split('?')[-1]
            query_dict = dict(parse_qsl(query_string))

            session_id = query_dict['session_id']
            user = USERS_STORE[session_id]

            with Path(__file__).with_name('widget.html').open('r') as f:
                # we use the widget.html file as a template/twig file
                template = Template(f.read())
                body = template.substitute({
                    'user_id': user['id'],
                    'redirect_url': query_dict['redirect_url'],
                    'client_id': CLIENT_ID,
                    'connection_id': str(int(time.monotonic())),
                    'account_id': str(time.monotonic()),
                    'balance_date': time.time(),
                })
                self._send_raw_html_response(body.encode('utf-8'))
            return

        #
        # Retrieve one Connection given connection
        #
        if self.path.startswith('/v2.1/connections/'):
            if not self._verify_auth():
                self._send_json_response(json={}, status_code=401)
                return
            connection_id = int(self.path.split('/')[-1])
            self._send_json_response(json={})
            return

        #
        # Getting a queue content. You must first call:
        # POST /v2.1/queues/subscribe to get a queue ID
        #
        if self.path.startswith('/v2.1/queues/'):
            token = self._verify_auth()
            if token is None:
                self._send_json_response(json={}, status_code=401)
                return
            user = token['user']

            # URL is like /v2.1/queues/{queue_id}?params...
            queue_id = self.path.split('/')[-1].split('?')[0]
            queue = user.get('queues', {}).get(queue_id)

            if queue is None:
                self._send_json_response(json={}, status_code=404)
                return

            # Only append the success event if the queue is not empty
            if queue:
                queue.append({
                    "id": "7",
                    "event_type": "SYNC_ALL_SUCCESS",
                    "resource_type": "CONNECTION",
                    "resource": {
                        "status": "SUCCESS"
                    }
                })

                # Update the checking account balance
                for one_account in user.get('accounts', []):
                    if one_account['type'] == 'CHECKINGS':
                        one_account['balance'] = str(Decimal(one_account['balance']) + Decimal('42'))

            self._send_json_response(json=queue, status_code=200)
            return

        #
        # Retrieve one account of the user identified by the token
        #
        if self.path.startswith('/v2.1/accounts/'):
            token = self._verify_auth()
            if token is None:
                self._send_json_response(json={}, status_code=401)
                return
            user = token['user']


            # get the account id as the last part of the path
            account_id = self.path.split('/')[-1]

            account = None
            for one_account in user['accounts']:
                if one_account['id'] == account_id:
                    account = one_account
                    break

            self._send_json_response(json=account)
            return

        #
        # Retrieve all the accounts of the user identified by the token
        #
        if self.path.startswith('/v2.1/accounts'):
            token = self._verify_auth()
            if token is None:
                self._send_json_response(json={}, status_code=401)
                return
            user = token['user']
            self._send_json_response(json=user['accounts'])
            return

        #
        # Retrieve all the accounts of the user identified by the token
        #
        if self.path.startswith('/v2.1/transactions'):
            token = self._verify_auth()
            if token is None:
                self._send_json_response(json={}, status_code=401)
                return
            user = token['user']
            self._send_json_response(json=user['transactions'])
            return

        self._send_raw_json_response(
            json_body=DATA_STORE[self.path]["body"],
            status_code=DATA_STORE[self.path]["status_code"],
        )
    
    def do_DELETE(self):
        """Answer to DELETE methods
        """

        if self.path.startswith('/v2.1/users/me'):
            # Note: it currently does not actually delete the user from the internal state
            self._send_json_response(json={}, status_code=200)
            return

        self._send_json_response(json={}, status_code=403)

    def do_POST(self):
        """Handle POST requests
        """
        body_size = int(self.headers["Content-Length"])
        body_string = self.rfile.read(body_size)


        #
        # Create Linxo User
        #
        if self.path.startswith('/v2.1/users'):
            if self._verify_auth() is None:
                self._send_json_response(json={}, status_code=401)
                return

            user_dict = json_module.loads(body_string)
            # cheap id generator
            user_id = str(len(USERS_STORE) + 1)
            user_dict['id'] = user_id
            # we index both by id and email, as for login
            # it make it easier to find back the user, as we are only provided the email
            USERS_STORE[user_id] = user_dict
            USERS_STORE[user_dict['email']] = user_dict

            # see https://developers.linxoconnect.com/reference-accounts-api/#tag/manage-users/operation/createUser
            self._send_json_response(
                {},
                status_code=201,
                headers={'location': '/users/' + str(user_id)}
            )

            return

        if self.path.startswith('/v2.1/queues/subscribe'):
            token = self._verify_auth()
            if token is None:
                self._send_json_response(json={}, status_code=401)
                return
            user = token['user']

            # cheap id generator
            if 'queues' not in user:
                user['queues'] = {}
            
            queue_id = 'mock_queue_id_'+str(len(user['queues']) + 1)
            user['queues'][queue_id] = [
                {
                    "id": "1",
                    "event_type": "SYNC_ALL_STARTED",
                    "resource_type": "CONNECTION",
                    "resource": {
                        "status": "SUCCESS"
                    }
                },
                {
                    "id": "2",
                    "event_type": "SYNC_ALL_LOGGING_IN",
                    "resource_type": "CONNECTION",
                    "resource": {
                        "status": "SUCCESS"
                    }
                },
                {
                    "id": "3",
                    "event_type": "SYNC_ALL_READING_ACCOUNT_LIST",
                    "resource_type": "CONNECTION",
                    "resource": {
                        "status": "SUCCESS"
                    }
                },
                {
                    "id": "4",
                    "event_type": "SYNC_ALL_READING_ACCOUNT",
                    "resource_type": "CONNECTION",
                    "resource": {
                        "status": "SUCCESS"
                    }
                },
                {
                    "id": "5",
                    "event_type": "SYNC_ALL_TRANSACTION_READ",
                    "resource_type": "CONNECTION",
                    "resource": {
                        "status": "SUCCESS"
                    }
                },
                {
                    "id": "6",
                    "event_type": "SYNC_ALL_LOGGING_OUT",
                    "resource_type": "CONNECTION",
                    "resource": {
                        "status": "SUCCESS"
                    }
                }
            ]

            self._send_json_response(
                {},
                status_code=201,
                headers={'location': '/queues/' + queue_id}
            )

            return

        #
        # Used for token authentification
        #

        if self.path.startswith('/token'):
            body_dict = dict(parse_qsl(body_string.decode('utf-8')))

            # used for Admin actions (create users etc.)
            if body_dict.get('grant_type') == 'client_credentials':

                authorization_header = self.headers.get("Authorization")
                client_id_and_client_secret = base64.decodebytes(
                    authorization_header[len("Basic ") :].encode()
                ).decode()
                client_id, client_secret = client_id_and_client_secret.split(":")

                if client_id != CLIENT_ID or client_secret != CLIENT_SECRET:
                    self._send_json_response(json={}, status_code=403)
                    return

                access_token = 'mock_admin_access_token_' + str(uuid.uuid4())
                ACCESS_TOKENS[access_token] = {}

                self._send_json_response({
                    'access_token': access_token,
                    'expires_in': 600,
                })
                return

            # used for user-specific actions (get one's bank account transaction etc.)
            if body_dict.get('grant_type') == 'password':
                if body_dict['client_id'] != CLIENT_ID or body_dict['client_secret'] != CLIENT_SECRET:
                    print("woot")
                    self._send_json_response(json={}, status_code=403)
                    return

                login_email = body_dict['username']
                user = USERS_STORE.get(login_email)
                if user is None or user['password'] !=  body_dict['password']:
                    self._send_json_response(json={}, status_code=403)
                    return

                access_token = 'mock_user_access_token_' + str(uuid.uuid4())
                refresh_token = 'mock_user_refresh_token_' + str(uuid.uuid4())
                ACCESS_TOKENS[access_token] = {
                    'refresh_token': refresh_token,
                    'user': user
                }

                self._send_json_response({
                    'access_token': access_token,
                    'refresh_token': refresh_token,
                    'expires_in': 600,
                })
                return


            # if it's not a grant type covered -> 403

            self._send_json_response(json={}, status_code=403)
            return

        #
        # Handle widget session
        #
        if self.path.startswith('/v2.1/widget/widget_session'):

            body_dict = dict(parse_qsl(body_string.decode('utf-8')))
            token_data = ACCESS_TOKENS.get(body_dict['access_token'])
            if token_data is None:
                self._send_json_response(json={}, status_code=401)
                return

            user = token_data['user']

            # we create the session id , and we fake that we also created a connection
            session_id = 'mock_widget_session_id_' + str(uuid.uuid4())
            user['session_id'] = session_id

            USERS_STORE[session_id] = user

            self._send_json_response({
                "session_id": session_id,
                "_links": {
                    "add_connection":"http://172.17.0.2:8087/v2.1/widget/add_connection?session_id=" + session_id
                }
            })
            return

        #
        # Handle user connection synchronization
        #
        if self.path.startswith('/v2.1/connections/') and self.path.endswith('/synchronize'):
            token = self._verify_auth()
            if token is None:
                self._send_json_response(json={}, status_code=401)
                return

            self._send_json_response(json={}, status_code=204)
            return

        #
        # Default on MOCK_INJECT data
        #

        self._send_raw_json_response(
            json_body=DATA_STORE[self.path]["body"],
            status_code=DATA_STORE[self.path]["status_code"],
        )

    def _verify_auth(self) -> Optional[Dict]:
        authorization_header = self.headers.get("Authorization")
        if not authorization_header.startswith('Bearer '):
            return False

        # we strip 'Bearer ' from the header to get the token
        access_token = authorization_header[len('Bearer '):]
        return ACCESS_TOKENS.get(access_token)

    #
    #  MOCK specific-part
    #
    #
    def do_MOCK_USER_DATA(self):
        """ Handle non-standard HTTP verb 'MOCK_USER_DATA', to inject in cache

        The reason of using a non-standard verb is that we're sure
        not to overlap with existing API mapping

        It's used to alter data in the USERS_STORE, it allows to have an easier
        mix with the "clever" part of this mock that replicate the business logic of Linxo
        """
        body_size = int(self.headers["Content-Length"])
        body_string = self.rfile.read(body_size)

        user_dict = json_module.loads(body_string)

        user_id = user_dict['id']

        # it handles both the case where the user was existing or not
        merged_user = USERS_STORE.get(user_id, {})
        merged_user.update(user_dict)

        # we update the index
        USERS_STORE[user_id] = merged_user
        USERS_STORE[merged_user['email']] = merged_user

        self._send_json_response(merged_user)

    def do_MOCK_INJECT(self):
        """ Handle non-standard HTTP verb 'MOCK_INJECT', to inject in cache

        The reason of using a non-standard verb is that we're sure
        not to overlap with existing API mapping

        It's used to prepopulate the given URL with the given data, so that
        the next GET requests on the very same URL (including get parameter)
        will get the given data in body
        """
        body_size = int(self.headers["Content-Length"])
        body_string = self.rfile.read(body_size)

        # allow the client to precise what will the status code
        # of its injected response
        # NOTE: if needed the same logic can be extended to other headers
        # as well
        status_code = int(self.headers.get("X-Mock-Status-Code", "200"))

        DATA_STORE[self.path] = {"status_code": status_code, "body": body_string}

        self.send_response(204)
        self.end_headers()

    def do_MOCK_FLUSH(self):
        """ Handle non-standard HTTP verb 'MOCK_FLUSH', to empty internal cache

        The reason of using a non-standard verb is that we're sure
        not to overlap with existing API mapping

        the URL given with this method is currently not used
        """

        USERS_STORE.clear()
        ACCESS_TOKENS.clear()
        DATA_STORE.clear()

        self.send_response(204)
        self.end_headers()


    def do_MOCK_DEBUG(self):
        """ Handle non-standard HTTP verb 'MOCK_DEBUG', to output DATASTORE

        the URL given with this method is currently not used
        """

        self._send_json_response({
            "data_store": DATA_STORE,
            "user_store": USERS_STORE
        })

    def _send_json_response(self, json: Union[Dict, List], status_code=200, headers={}):
        self._send_raw_json_response(
            json_body=json_module.dumps(json).encode("utf-8"),
            status_code=status_code,
            headers=headers
        )

    def _send_raw_json_response(self, json_body: bytes, status_code=200, headers={}):
        self.send_response(status_code)
        self.send_header("Content-Type", "application/json")
        for name, value in headers.items():
            self.send_header(name, value)
        self.end_headers()
        self.wfile.write(json_body)

    def _send_raw_html_response(self, html_body: bytes, status_code=200, headers={}):
        self.send_response(status_code)
        self.send_header("Content-Type", "text/html")
        for name, value in headers.items():
            self.send_header(name, value)
        self.end_headers()
        self.wfile.write(html_body)

    def _send_blank_response(self, status_code):
        self.send_response(status_code)
        self.end_headers()

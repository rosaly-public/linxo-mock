FROM python:3.8-alpine
COPY mock mock/
ENV MOCK_LINXO_PORT 8087
EXPOSE 8087
# -u is for unbuffered output
# otherwise you will not directly see the stacktrace in the logs when an error occur
# having it unbuffered ease debugging
CMD ["python", "-u", "-m" , "mock"]

# linxo-mock

Supported endpoints:

```
GET /v2.1/providers
GET /v2.1/widget/add_connection
GET /v2.1/connections/...
GET /v2.1/queues/...
GET /v2.1/accounts
GET /v2.1/transactions
POST /v2.1/users
POST /v2.1/queues/subscribe
POST /token
POST /v2.1/widget/widget_session
DELETE /v2.1/users/me
MOCK_USER_DATA *
MOCK_INJECT *
MOCK_FLUSH *
MOCK_DEBUG *
```

### Building

Requirements:

- You must be connected to the `leonsia` DockerHub account
- Have <https://github.com/docker/buildx> installed if not already the case

Create a Docker builder that will be able to build multi-arch:

```
docker buildx create --name leonsia --bootstrap --use
```

Now, you can build the image:

```

docker buildx build \
    --push \
    --platform linux/arm64/v8,linux/amd64 \
    --tag leonsia/mock-linxo .
```
